<h1> Application de Chat Multi-Thread en Java </h1>

Cette Application a été développée en utilisant Java. Pour démarrer il faut juste exécuter tout d'abord le fichier <b> Server.java </b> et puis exécuter le fichier <b> Client.java </b> autant de fois qu'on veut ajouter des utilisateurs au serveur. De cette façon, on va établir une connexion avec le Serveur et on pourra créer un Chat en groupe avec plusieurs utilisateurs.

Pour obtenir plus de détails sur l'architecture de l'application, vous pouvez consulter le document <b> rapport.pdf </b> dans le repertoire  <b> rapport</b> où j'explique plus en détail comment ce petit projet a été développé ainsi que des scénarios d'utilisation entre autres.

Voici l'UML de l'architecture utilisé dans le projet:

<img src="Rapport/uml.png"
     alt="UML"
     style="float: left; margin-right: 10px;" />