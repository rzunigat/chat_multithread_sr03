package client;


import java.io.IOException;
import java.net.Socket;


/**
 * Client class to be connected to the HOST ip and port PORT
 * 
 * @author Uriel
 *
 */
public class Client {

	/**
	 * HOST where the client will be connected
	 */
	private final String HOST = "127.0.0.1";

	/**
	 * PORT through which the server listens
	 */
	private final int PORT = 4000;

	/**
	 * Thread to Receive Messages by the server
	 */
	private ClientMessageReceptor receptor;

	/**
	 * Thread to Send Messages to the server
	 */
	private ClientMessageSender sender;

	/**
	 * Main Function that will be executed when starting this Java File
	 * 
	 * @param args
	 */
	public static void main(String[] args) {

		Client client = new Client();
		client.StartThreads();

	}

	/**
	 * Constructor by Default
	 */
	public Client() {

		try {
			Socket sc = new Socket(HOST, PORT);

			this.sender = new ClientMessageSender(sc);
			this.receptor = new ClientMessageReceptor(sc);

		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	/**
	 * Starts the threads to send and receive messages from the server
	 */
	public void StartThreads() {

		this.receptor.start();
		this.sender.start();

	}

}
