package client;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.SocketException;
import java.util.Scanner;

import com.fasterxml.jackson.databind.ObjectMapper;

import manager.ServerController;
import message.Message;

/**
 * Write thread for the client. This process is responsible for executing an
 * infinite loop until the disconnection of a client
 * 
 * @author Uriel
 *
 */
public class ClientMessageSender extends Thread {

	/**
	 * Server Socket linked to the Thread
	 */
	private Socket server;

	/*
	 * Read the lines writes by the user in console
	 */
	private Scanner scan;

	/**
	 * Current Message written by the user
	 */
	private String messageContent;

	/**
	 * Nickname linked to this Client
	 */
	private String nickname;

	/**
	 * Recognize if it's the first message sent (Case of the Nickname)
	 */
	private boolean isStarted;

	/**
	 * Class that allows transforming a string into a JSON string and vice versa
	 */
	ObjectMapper mapper;

	/**
	 * Message received by the InputDataStream
	 */
	Message message;

	/**
	 * Constructor
	 * 
	 * @param _server linked to the Thread
	 */
	public ClientMessageSender(Socket _server) {
		this.server = _server;
		this.scan = new Scanner(System.in);
		this.messageContent = "";
		this.nickname = "";
		this.isStarted = false;

		mapper = new ObjectMapper();
		message = new Message();
	}

	/**
	 * Run the current Thread
	 */
	@Override
	public void run() {

		try {
			DataOutputStream out = new DataOutputStream(server.getOutputStream());

			while (message.getCode() != Message.ERROR
					&& (message.getCode() != Message.DISCONNECT || !message.getSender().equals(nickname))) {

				if (!isStarted) {

					System.out.print("Write your nickname: ");
					messageContent = scan.nextLine();

					while (!ServerController.IsValide(messageContent)) {

						System.out.println("This Nickname is Already on the System");

						System.out.print("Write another nickname: ");
						messageContent = scan.nextLine();

					}

					message.setCode(Message.SUCCESS);
					message.setContent(messageContent);
					message.setSender(messageContent);

					nickname = message.getContent();

					out.writeUTF(message.toString());

					this.isStarted = true;

				}

				else {

					message.setContent(scan.nextLine());

					if (message.getContent().contentEquals("exit")) {

						message.setCode(Message.DISCONNECT);

					}

					out.writeUTF(message.toString());

				}

			}

			this.server.shutdownOutput();
			;

		} catch (SocketException e) {

			this.interrupt();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
