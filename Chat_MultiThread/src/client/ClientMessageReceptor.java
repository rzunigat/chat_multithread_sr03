package client;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.SocketException;
import com.fasterxml.jackson.databind.ObjectMapper;
import message.Message;

/**
 * 
 * @author Uriel
 *
 */
public class ClientMessageReceptor extends Thread {

	/**
	 * Server Socket linked to the Thread
	 */
	private Socket server;

	/**
	 * Current Message Content Received in the DataInputStream
	 */
	private String messageContent;

	/**
	 * Message received by the DataInputStream
	 */
	private Message message;

	/**
	 * Variable to know if is the first message
	 */
	private boolean isStarted;

	/**
	 * Nickname of the Client Associated
	 */
	private String nickname;

	/**
	 * Constructor
	 * 
	 * @param _server Socket linked to the Thread
	 */
	public ClientMessageReceptor(Socket _server) {
		this.server = _server;
		this.messageContent = "";
		this.message = new Message();
		this.isStarted = false;
		this.nickname = "";
	}

	/**
	 * Execute the current Thread
	 */
	@Override
	public void run() {

		try {
			DataInputStream in = new DataInputStream(server.getInputStream());

			while (message.getCode() != Message.ERROR
					&& (message.getCode() != Message.DISCONNECT || !message.getSender().equals(nickname))) {

				messageContent = in.readUTF();

				message = read(messageContent);

				if (!isStarted) {

					this.nickname = message.getSender();
					isStarted = true;

				}

				if (message.getCode() == Message.ADDED || message.getCode() == Message.DISCONNECT) {

					System.out.println(message.getContent());

				}

				else {

					System.out.println(message.getSender() + ": " + message.getContent());
				}

			}

			this.server.shutdownInput();
			;

		} catch (SocketException e) {

			System.out.println("Server Disconnected ... ");
			this.interrupt();

		} catch (IOException e) {

			System.out.println("This client already exists on the DB ... :( ");
		}
	}

	/**
	 * Converts a JSON String into Message Object
	 * 
	 * @param jsonMessage : To convert into a Message Object
	 * @return a Message Object linked to the jsonMessage
	 */
	private Message read(String jsonMessage) {

		Message m = null;
		ObjectMapper mapper = new ObjectMapper();

		try {
			m = mapper.readValue(jsonMessage, Message.class);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return m;
	}
}
