package message;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * 
 * Message that will be sent and received during 
 * the use of the chat by the clients and the server
 * 
 * @author Uriel
 *
 */
public class Message {
	
	/**
	 * Nickname of client who send the message
	 * 
	 */
	private String sender;
	
	/**
	 * 
	 * Content of the message
	 */
	private String content;
	
	/**
	 * 
	 * Code that will allow you to know the status of the message
	 */
	private int code;
	
	/**
	 * Constant Code Success
	 */
	public static int SUCCESS = 200;
	
	/**
	 * Constant Code Added
	 */
	public static int ADDED = 201;
	
	/**
	 * Constant Code Refuse
	 */
	public static int REFUSE = 400;
	
	/**
	 * Constant Code Error
	 */
	public static int ERROR = 404;
	
	/**
	 * Constant Code Disconnect
	 */
	public static int DISCONNECT = 405;
	
	/**
	 * 
	 * Contructor by default
	 */
	public Message() {
		this.sender = "";
		this.content = "";
		this.code = 0;
	}
	
	/**
	 * Contructor
	 * 
	 * @param _sender : Nickname of the client who send the message
	 * @param _content : Content of the message
	 * @param _code : Code that will allow you to know the status of the message
	 */
	public Message(String _sender, String _content, int _code){
		
		this.sender = _sender;
		this.content = _content;
		this.code = _code;
			
	}
	
	/**
	 * Getter Code
	 * 
	 * @return the code message
	 */
	public int getCode() {
		return code;
	}
	
	/**
	 * Getter Content
	 * 
	 * @return the content message
	 */
	public String getContent() {
		return content;
	}
	
	/**
	 * Getter Sender
	 * 
	 * @return the nickname of the client
	 */
	public String getSender() {
		return sender;
	}
	
	/**
	 * Setter Code
	 * 
	 * @param Code Message
	 */
	public void setCode(int code) {
		this.code = code;
	}
	
	/**
	 * 
	 * Setter Content 
	 * 
	 * @param content of the message
	 */
	
	public void setContent(String content) {
		this.content = content;
	}
	
	/**
	 * Setter Sender 
	 * 
	 * @param sender message
	 */
	
	public void setSender(String sender) {
		this.sender = sender;
	}
	
	/**
	 * 
	 * Transform the class to a Text string of type JSON
	 * 
	 * @return a text string transformed into JSON
	 */
	@Override
	public String toString() {
		
		ObjectMapper mapper = new ObjectMapper();
		
		try {
			return mapper.writeValueAsString(this);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "Error";
		}

	}

}
