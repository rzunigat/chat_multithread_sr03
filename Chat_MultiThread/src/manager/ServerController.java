package manager;

import java.io.DataOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import server.ServerMessageReceptor;

/**
 * ServerController is a class with static methods and attributes that
 * facilitates message propagation, client aggregation and delete of them
 * 
 * @author Uriel
 * 
 */
public class ServerController {
	/**
	 * The structure data that I'll use to manage all the clients. with the HashMap,
	 * I can avoid repeating nicknames
	 * 
	 */
	public static HashMap<String, ServerMessageReceptor> clients = new HashMap<String, ServerMessageReceptor>();

	/**
	 * <p>
	 * Show The list of all the clients: Nickname: Rodrigo Socket: ... ...
	 * </p>
	 * 
	 */
	public static void ShowAllClients() {

		Iterator it = clients.entrySet().iterator();
		System.out.println(" -------------------------------------------------------------- ");

		while (it.hasNext()) {

			Map.Entry<String, ServerMessageReceptor> client = (Map.Entry) it.next();
			System.out.println("Nickname: " + client.getKey() + " Socket: " + client.getValue().getClient());

		}

		System.out.println(" -------------------------------------------------------------- ");

	}

	/**
	 * Return true or false if the value entered as a parameter is within the data
	 * structure or not.
	 * 
	 * @param key: The nickname to verify on the HashMap
	 * @return true if the nickname isn't in the HashMap and false if not
	 */
	public static boolean IsValide(String key) {

		if (clients.containsKey(key))
			return false;

		return true;

	}

	/**
	 * <p>
	 * This method add a new element to the hashMap. If it's possible, the method
	 * returns true and false if not.
	 * </p>
	 * 
	 * @param nickname: The client identifier
	 * @param socket:   Socket associated with that nickname
	 * @return returns true if the element was added to the data structure and false
	 *         if the nickname already exists.
	 */

	public static boolean AddClient(String nickname, ServerMessageReceptor socket) {

		if (IsValide(nickname)) {

			clients.put(nickname, socket);
			return true;

		}

		else {

			return false;

		}

	}

	/**
	 * <p>
	 * Remove a Client element of the HasMap <String, ServerMessageReceptor>
	 * </p>
	 * 
	 * @param nickname to remove of the HashMap
	 * @return true if any error is found
	 */
	public static boolean RemoveClient(String nickname) {

		ServerMessageReceptor value = clients.remove(nickname);

		if (value == null)
			return false;

		else {

			value.interrupt();

			return true;

		}

	}

	/**
	 * Send a message to all the clients stored on the HashMap
	 * 
	 * @param message to send to all the clients
	 */
	public static void SendMessageToAllClients(String message) {

		DataOutputStream out;

		Iterator it = clients.entrySet().iterator();

		while (it.hasNext()) {

			Map.Entry<String, ServerMessageReceptor> client = (Map.Entry) it.next();

			out = client.getValue().getOut();

			try {
				out.writeUTF(message);
			} catch (IOException e) {

				e.printStackTrace();
			}

		}

	}

}
