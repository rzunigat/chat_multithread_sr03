package server;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.SocketException;
import com.fasterxml.jackson.databind.ObjectMapper;

import manager.ServerController;
import message.Message;

/**
 * Listening thread for the Server. This process is responsible for executing an
 * infinite loop until the disconnection of a client.
 * 
 * Every time it receives a message, propagate the message across all clients
 * with the help of the ServerController class
 * 
 * @author Uriel
 *
 */
public class ServerMessageReceptor extends Thread {

	private String nickname;

	/**
	 * Client Linked to the Thread
	 */
	private Socket client;

	/**
	 * Recognize if it is the first message received
	 */
	private boolean isStarted;

	/**
	 * Current message String read in the InputDataStream Client
	 */
	private String messageContent;

	/**
	 * DataOutputStream linked to the Socket client
	 */
	private DataOutputStream out;

	/**
	 * DataInputStream linked to the Socket client
	 */
	private DataInputStream in;

	/**
	 * Message received by the InputDataStream
	 */
	private Message message;

	/**
	 * Constructor
	 * 
	 * @param _client linked to the Thread
	 */
	public ServerMessageReceptor(Socket _client) {
		this.client = _client;
		this.nickname = "";
		this.isStarted = false;
		this.messageContent = "";
		this.message = new Message();

		try {
			in = new DataInputStream(this.client.getInputStream());
			out = new DataOutputStream(this.client.getOutputStream());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * Getter Client
	 * 
	 * @return the Client Socket
	 */

	public Socket getClient() {
		return client;
	}

	/**
	 * Getter Out
	 * 
	 * @return the DataOutputStream linked to the Socket Client
	 */
	public DataOutputStream getOut() {
		return out;
	}

	/**
	 * Run the Current Thread
	 */
	@Override
	public void run() {
		try {

			while (message.getCode() != Message.ERROR) {

				messageContent = in.readUTF();

				message = read(messageContent);

				if (!isStarted) {

					// Cliente Conectado y Agregado al HashMap
					if (ServerController.AddClient(message.getSender(), this)) {

						message.setCode(Message.ADDED);

						message.setContent("\n------------------------------------------------------- \n"
								+ message.getSender() + " has rejoint the conversation \n"
								+ "------------------------------------------------------- \n");

						nickname = message.getSender();
						ServerController.SendMessageToAllClients(message.toString());

					}

					// No se pudo agregar el Cliente al Servidor
					else {

						System.out.println("Connection Failed: Nickname already exists on the Server ... ");

						message.setCode(Message.REFUSE);

						out.writeUTF(message.toString());
						this.client.close();
						break;

					}

					isStarted = true;

				}

				else {

					if (message.getCode() == Message.DISCONNECT) {

						message.setContent("\n------------------------------------------------------- \n"
								+ message.getSender() + " has left the conversation \n"
								+ "------------------------------------------------------- \n");

						ServerController.SendMessageToAllClients(message.toString());

						ServerController.RemoveClient(message.getSender());

					}

					ServerController.SendMessageToAllClients(message.toString());

				}

			}

			System.out.println("Socket termin�e: " + this.client.toString());
			this.client.close();

		} catch (SocketException e) {

			// Exception Bad Connection

			message.setCode(Message.SUCCESS);
			message.setContent("\n------------------------------------------------------- \n "
								+ "Client: " + this.nickname + " Disconnected without write - exit - \n"
								+ "------------------------------------------------------- \n" );
			message.setSender("Server");

			if (this.isStarted) {

				if (ServerController.RemoveClient(this.nickname)) {


					if (!ServerController.clients.isEmpty()) {
						ServerController.SendMessageToAllClients(message.toString());
					}

				}
			}

		} catch (IOException e) {

		}
	}

	/**
	 * Transform a JSON String into Message object
	 * 
	 * @param jsonMessage : To tranform into Message Object
	 * @return a Message Object linked to the jsonMessage
	 */
	private Message read(String jsonMessage) {

		Message m = null;
		ObjectMapper mapper = new ObjectMapper();

		try {
			m = mapper.readValue(jsonMessage, Message.class);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return m;
	}

}
