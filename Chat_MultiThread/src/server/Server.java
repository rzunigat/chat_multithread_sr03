package server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;


/**
 * Server class to which all users will be connected. This class carries a
 * static main method which will be called when running this java file
 * 
 * @author Uriel
 *
 */
public class Server {

	/**
	 * Server listening port
	 */
	private final int PORT = 4000;

	/**
	 * Last client connected to the server
	 */
	private Socket currentClient;

	/**
	 * Last listening thread associated with the last connected client
	 */
	private ServerMessageReceptor currentServerMessageReceptor;

	/**
	 * Socket server connection
	 */
	private ServerSocket conn;

	/**
	 * Main function to be called when starting the program
	 * 
	 * @param args
	 */
	public static void main(String[] args) {

		Server server = new Server();
		server.ReceiveClients();

	}

	/**
	 * Constructor by default
	 */
	public Server() {

		this.currentClient = new Socket();

		try {

			this.conn = new ServerSocket(PORT);

		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	/**
	 * Run an infinite loop that receives clients and creates listener threads for
	 * each of them
	 */
	public void ReceiveClients() {

		while (true) {

			try {

				this.currentClient = this.conn.accept();

			} catch (IOException e) {

				e.printStackTrace();
			}

			this.currentServerMessageReceptor = new ServerMessageReceptor(this.currentClient);
			this.currentServerMessageReceptor.start();

		}

	}

}
